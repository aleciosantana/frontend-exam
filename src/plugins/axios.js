
import Vue from 'vue';
import axios from 'axios';

// Full config:  https://github.com/axios/axios#request-config
// axios.defaults.baseURL = process.env.baseURL || process.env.apiUrl || '';
// axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;
// axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

const iAaxios = axios.create({
  baseURL: 'http://localhost:3000/',
  // timeout: 60 * 1000, // Timeout
  // withCredentials: true, // Check cross-site Access-Control
});

iAaxios.interceptors.request.use(config => config, error => Promise.reject(error));

// Add a response interceptor
iAaxios.interceptors.response.use(response => response, error => Promise.reject(error));

const Plugin = {
  install: (iVue) => {
    Object.assign(iVue, { axios: iAaxios });

    window.axios = iAaxios;
    Object.defineProperties(iVue.prototype, {
      axios: {
        get() {
          return iAaxios;
        },
      },
      $axios: {
        get() {
          return iAaxios;
        },
      },
    });
  },
};

Vue.use(Plugin);

export default Plugin;
