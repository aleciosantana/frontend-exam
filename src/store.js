import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const asc = ({ order: a }, { order: b }) => a - b;

export default new Vuex.Store({
  state: {
    stdTodo: {},
    todos: [],
  },
  mutations: {
    SET_STD_TODO(state, todo) {
      const stdTodo = { ...todo };
      Object.assign(state, { stdTodo });
    },
    CLEAR_STD_TODO(state) {
      Object.assign(state, { stdTodo: {} });
    },

    // TODOS/TODO
    SET_TODOS(state, todos) {
      Object.assign(state, { todos });
    },
    ADD_TODO({ todos }, todo) {
      todos.push(todo);
    },
    REMOVE_TODO({ todos }, id) {
      const index = todos.findIndex(({ id: key }) => key === id);
      todos.splice(index, 1);
    },
    UPDATE_TODO({ todos }, newtodo) {
      const index = todos.findIndex(({ id }) => id === newtodo.id);
      todos.splice(index, 1, newtodo);
    },
  },
  actions: {
    setStdTodo({ commit }, { todo }) {
      commit('SET_STD_TODO', todo);
    },
    clearStdTodo({ commit }) {
      commit('CLEAR_STD_TODO');
    },

    // todos/todo
    setTodos({ commit }, { todos }) {
      commit('SET_TODOS', todos);
    },
    addTodo({ commit }, { todo }) {
      commit('ADD_TODO', todo);
    },
    removeTodo({ commit }, { id }) {
      commit('REMOVE_TODO', id);
    },
    updateTudo({ commit }, { todo }) {
      commit('UPDATE_TODO', todo);
    },
  },
  getters: {
    stdTodo: ({ stdTodo }) => stdTodo,
    todos: ({ todos }) => todos,
    completeds({ todos }) {
      const complete = ({ completed }) => completed;

      return todos.filter(complete).sort(asc); // completos
    },
    incompletes({ todos }) {
      const incomplete = ({ completed }) => !completed;

      return todos.filter(incomplete).sort(asc); // não completos
    },
  },
});
