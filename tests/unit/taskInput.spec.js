import { shallowMount } from '@vue/test-utils';
import TaskList from '@/components/TaskInput.vue';

import Vue from 'vue';
import Vuetify from 'vuetify';

Vue.use(Vuetify);

describe('TaskInpu.vue', () => {
  //
  const wrapper = shallowMount(TaskList, {
    propsData: {
      type: '',
      value: {
        description: '',
      },
    },
  });

  it('Is Vue instance', () => {
    expect(wrapper.isVueInstance()).toBeTruthy();
  });

  let task;
  beforeEach(() => {
    task = wrapper.find('#task');
  });

  it('Label is "Nova Tarefa" when the type is "add"', () => {
    wrapper.setProps({ type: 'add' });

    expect(task.attributes('label')).toBe('Nova Tarefa');
  });

  it('Label is "Editar Tarefa" when the type is "edit"', () => {
    wrapper.setProps({ type: 'edit' });

    expect(task.attributes('label')).toBe('Editar Tarefa');
  });

  it('Label is "Filtrar Tarefas" when the type is "search"', () => {
    wrapper.setProps({ type: 'search' });

    expect(task.attributes('label')).toBe('Filtrar Tarefas');
  });
});
